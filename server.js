var express = require('express');
var app = express();

app.use(express.cookieParser());
app.use(express.session({secret: '098f6bcd4621d373'}));
app.use(express.bodyParser());

var exec = require('child_process').exec;
var sys = require('sys');
var crypto = require('crypto');
var fs = require('fs');


function find_user_line(name, line, callback){
  var data = fs.readFileSync("login_data.txt", 'utf8');
  var lines = data.split("\n");
  if(line >= lines.length){
    callback(false);
  }else{
    if(lines[line].split(":")[0] != name){
      find_user_line(name, line+1, function(cb_data){callback(cb_data);});
    }else{
      callback(lines[line]);
    }
  }
}


app.get('/', function(req, res) {
  res.sendfile(__dirname + "/index.html");
});

app.post('/home', function(req, res) {
    var name = req.body.name;
    var pw = req.body.pw;
    
    find_user_line(name, 0, function(line){
      if(line === false){
        res.end('false');
      }else{
        line_parts = line.split(":");
        if((line_parts[1] == pw && line_parts[0] == name) || req.session.logged){
          req.session.logged = true;
          req.session.user = name;
          console.log("user "+name+" logged in.");
          res.sendfile(__dirname + "/home.html");
        }else{
          res.end('false');
        }
      }
    });
});

app.post('/session', function(req, res) {
    if(req.session.logged == true){
      res.sendfile(__dirname + "/home.html");
    }else{
      res.end('false');
    }
});

//assets
app.get('/md5.js', function(req, res) {
  res.sendfile(__dirname + "/md5.js");
});
app.get('/icon.css', function(req, res) {
  res.sendfile(__dirname + "/icon.css");
});
app.get('/jquery.js', function(req, res) {
  res.sendfile(__dirname + "/jquery.js");
});

//deliver the json object with all saved configuration
app.get('/get_json', function(req, res) {
  console.log("sending json");
  res.sendfile(__dirname + "/dip."+req.session.user+".json");
});

//deliver the json object with all saved configuration
app.post('/receive_json', function(req, res) {
  var j_data_old = JSON.parse(fs.readFileSync('dip.'+req.session.user+'.json'));

  var j_data = req.body;
  var exec_array = new Array();
  console.log("received json");

  for (var name in j_data) {
    //only send if status was changed
    if(j_data_old[name] != undefined){
      if(j_data_old[name].status != j_data[name].status){
        var all_dip = "";
        for (var dip_name in j_data[name].dip) {
          all_dip += ' '+j_data[name].dip[dip_name];
        }
        if(j_data[name].status == "on"){
          dip_status = " 1";
        }else{
          dip_status = " 0";
        }
        exec_array.push("433/send 23"+all_dip+dip_status);
      }
    }
  }
  if(exec_array.length > 0){
    exec_this_array(exec_array);
  }
  fs.writeFile("dip."+req.session.user+".json", JSON.stringify(j_data, null, 2));
  res.end('it worked');
});





app.listen(process.env.PORT || 8081);
console.log("Server is running...");


function exec_this_array(arr){
  console.log("executing num "+arr.length+": "+arr[arr.length-1]);
  exec(arr[arr.length-1], function() {
    arr.splice(-1,1);
    if(arr.length > 0){
      exec_this_array(arr);
    }
  });
}